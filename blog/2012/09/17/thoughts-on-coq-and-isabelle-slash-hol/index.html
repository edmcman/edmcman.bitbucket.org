
<!DOCTYPE html>
<!--[if IEMobile 7 ]><html class="no-js iem7"><![endif]-->
<!--[if lt IE 9]><html class="no-js lte-ie8"><![endif]-->
<!--[if (gt IE 8)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
  <meta charset="utf-8">
  <title>Thoughts on Coq and Isabelle/HOL - Ed's Blog</title>
  <meta name="author" content="Ed Schwartz">

  
  <meta name="description" content="Coq and Isabelle/HOL are both &#8220;proof assistants&#8221; or &#8220;interactive
theorem provers&#8221;. Both systems allows one to model a theorem &hellip;">
  

  <!-- http://t.co/dKP3o1e -->
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  
  <link rel="canonical" href="http://edmcman.bitbucket.org/blog/2012/09/17/thoughts-on-coq-and-isabelle-slash-hol">
  <link href="/favicon.png" rel="icon">
  <link href="/stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css">
  <script src="/javascripts/modernizr-2.0.js"></script>
  <script src="/javascripts/ender.js"></script>
  <script src="/javascripts/octopress.js" type="text/javascript"></script>
  <link href="/atom.xml" rel="alternate" title="Ed's Blog" type="application/atom+xml">
  <!--Fonts from Google"s Web font directory at http://google.com/webfonts -->
<link href="http://fonts.googleapis.com/css?family=PT+Serif:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css">

  

</head>

<body   >
  <header role="banner"><hgroup>
  <h1><a href="/">Ed's Blog</a></h1>
  
    <h2>A PhD Student's Musings</h2>
  
</hgroup>

</header>
  <nav role="navigation"><ul class="subscription" data-subscription="rss">
  <li><a href="/atom.xml" rel="subscribe-rss" title="subscribe via RSS">RSS</a></li>
  
</ul>
  
<form action="http://google.com/search" method="get">
  <fieldset role="search">
    <input type="hidden" name="q" value="site:edmcman.bitbucket.org" />
    <input class="search" type="text" name="q" results="0" placeholder="Search"/>
  </fieldset>
</form>
  
<ul class="main-navigation">
  <li><a href="/">Blog</a></li>
  <li><a href="/blog/archives">Archives</a></li>
</ul>

</nav>
  <div id="main">
    <div id="content">
      <div>











<article class="hentry" role="article" >
  
  <header>
    <h1 class="entry-title">

Thoughts on Coq and Isabelle/HOL

</h1>

    
      <p class="meta">
        








  


<time datetime="2012-09-17T10:55:00-04:00" pubdate data-updated="true">Sep 17<span>th</span>, 2012</time>
         &bull; <a rel="bookmark" href="/blog/2012/09/17/thoughts-on-coq-and-isabelle-slash-hol/">&infin;</a>
      </p>
    
  </header>

<div class="entry-content"><p>Coq and Isabelle/HOL are both &#8220;proof assistants&#8221; or &#8220;interactive
theorem provers&#8221;.  Both systems allows one to model a theorem, and
then prove the theorem true.  The resulting theorem is then checked,
typically by reducing the proof to a program that can be type checked.
Type checkers are relatively simple, and so we are confident that
these machine checked proofs are correct.</p>

<p>I&#8217;ve been reading <a href="http://www.cis.upenn.edu/~bcpierce/sf/">Software Foundations</a>, which is an
introduction to program semantics and verification in which everything
is modeled and proved in Coq.  Remember doing those dreaded structural
induction program semantics proofs by hand?  It&#8217;s a little bit more
satisfying when done in Coq, and sometimes is easier too.  More
recently, I have been learning Isabelle/HOL.  Technically, Isabelle is
a proof assistant that allows multiple logics to be built on top of
it, including HOL, or High Order Logic.</p>

<p>Unlike Isabelle, Coq only supports High Order Logic natively.  Coq has
support for <code>\forall</code> and <code>--&gt;</code> baked in to it.  <code>--&gt;</code> is used to
represent both entailment and logical implication.  As a result, it&#8217;s
very easy to write down theorems, because there is generally only one
way to write them!  Isabelle/HOL, being split up into Isabelle&#8217;s
meta-logic and HOL&#8217;s logic, is different.  Isabelle has a notion of a
universal quantifier <code>/\</code> and entailment <code>==&gt;</code> baked in.  HOL then
models the <code>\forall</code> quantifier, and logical implication <code>--&gt;</code>.  Of
course, the two levels are closely related, and it is possible to
switch between them.  However, when writing a theorem, it is often not
clear in which level quantification and entailment or implication
should be written.</p>

<p>In my first proofs, I errored on the side of HOL instead of the
metalogic, only to find that some of Isabelle&#8217;s utilities only work on
the metalogic level.  For instance, there is no quick way to
instantiate <code>p</code> and <code>q</code> in <code>\forall p q. P p q</code>.  There is a syntax,
however, for instantiating <code>p</code> and <code>q</code> in the meta-logic quantified
version.  The ambiguity between logics is a very inelegant property of
Isabelle/HOL in my opinion.</p>

<p>Isabelle does have some nice features, though.  It has a structured
proof language called Isar.  The idea is to allow Isabelle proofs to
look like real, human proofs.  (In contrast, non-structural mechanized
proofs are just a series of tactics, which do not explain <em>why</em> a
theorem is true.)  Here&#8217;s a simple example:</p>

<figure class='code'><div class="highlight"><table><tr><td class="gutter"><pre class="line-numbers"><span class='line-number'>1</span>
<span class='line-number'>2</span>
<span class='line-number'>3</span>
<span class='line-number'>4</span>
<span class='line-number'>5</span>
<span class='line-number'>6</span>
<span class='line-number'>7</span>
<span class='line-number'>8</span>
<span class='line-number'>9</span>
</pre></td><td class='code'><pre><code class=''><span class='line'>  case (Assert bexp)
</span><span class='line'>  assume eq: "bval p st = bval q st"
</span><span class='line'>  show ?case
</span><span class='line'>    proof -
</span><span class='line'>      have "bval (fse (Assert bexp) p) st = bval (And p bexp) st" by simp
</span><span class='line'>      also from eq have "... = bval (And q bexp) st" by (rule and_equiv)
</span><span class='line'>      also have "... = bval (fse (Assert bexp) q) st" by simp
</span><span class='line'>      finally show "?thesis" .
</span><span class='line'>    qed</span></code></pre></td></tr></table></div></figure>


<p>I bet you can figure out what&#8217;s going on, even though you don&#8217;t know the syntax.</p>

<p>My last observation about Isabelle/HOL and Coq has to do with their
unspoken strategies.  Isabelle/HOL seems to be designed to make
extensive use of simplification rules.  Indeed the tutorial
essentially says to prove something, you should write your theorem,
try to prove it automatically, and if that fails, add a simplification
lemma and repeat!  This is nice when it works.  However, manipulating
rules manually is difficult in my opinion, although that could be
because I&#8217;m a novice user.  Manually manipulating rules in Coq is much
easier; the design of tactics just seems to allow me to do what I want
more naturally.  On the other hand, I&#8217;ve been using Coq for longer.</p>

<p>In conclusion, Coq and Isabelle/HOL are both interesting tools, and
both can probably accomplish what you want.  The differences are
subtle, and the best way to get a feel for them is to learn them!  I
recommend <a href="http://www.cis.upenn.edu/~bcpierce/sf/">Software Foundations</a> for Coq, and the
<a href="http://www.cl.cam.ac.uk/research/hvg/isabelle/dist/Isabelle2012/doc/tutorial.pdf">tutorial</a>
for Isabelle/HOL.</p>
</div>


  <footer>
    <p class="meta">
      
  

<span class="byline author vcard">Posted by <span class="fn">Ed Schwartz</span></span>

      








  


<time datetime="2012-09-17T10:55:00-04:00" pubdate data-updated="true">Sep 17<span>th</span>, 2012</time>
      


    </p>
    
      <div class="sharing">
  
  <a href="http://twitter.com/share" class="twitter-share-button" data-url="http://edmcman.bitbucket.org/blog/2012/09/17/thoughts-on-coq-and-isabelle-slash-hol/" data-via="edmcman" data-counturl="http://edmcman.bitbucket.org/blog/2012/09/17/thoughts-on-coq-and-isabelle-slash-hol/" >Tweet</a>
  
  
  <div class="g-plusone" data-size="medium"></div>
  
  
    <div class="fb-like" data-send="true" data-width="450" data-show-faces="false"></div>
  
</div>

    
    <p class="meta">
      
        <a class="basic-alignment left" href="/blog/2012/09/07/undocumented-features-of-vsa/" title="Previous Post: Undocumented Features of VSA">&laquo; Undocumented Features of VSA</a>
      
      
        <a class="basic-alignment right" href="/blog/2012/09/24/success/" title="Next Post: Success?">Success? &raquo;</a>
      
    </p>
  </footer>
</article>

  <section>
    <h1>Comments</h1>
    <div id="disqus_thread" aria-live="polite"><noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
</div>
  </section>

</div>

<aside class="sidebar">
  
    <section>
  <h1>Recent Posts</h1>
  <ul id="recent_posts">
    
      <li class="post">
        <a href="/blog/2014/02/28/foxfipdanet-on-mavericks/">FoxFi/PdaNet on Mavericks</a>
      </li>
    
      <li class="post">
        <a href="/blog/2013/05/16/experimental-methodology/">My Experimental Methodology</a>
      </li>
    
      <li class="post">
        <a href="/blog/2013/05/05/coming-to-a-bap-near-you/">Coming to a BAP near you</a>
      </li>
    
      <li class="post">
        <a href="/blog/2013/03/28/typechecking-fail/">Typechecking fail</a>
      </li>
    
      <li class="post">
        <a href="/blog/2013/02/27/bap-for-everyone/">BAP for everyone</a>
      </li>
    
  </ul>
</section>


<section>
  <h1>Latest Tweets</h1>
  <ul id="tweets">
    <li class="loading">Status updating...</li>
  </ul>
  <script type="text/javascript">
    $.domReady(function(){
      getTwitterFeed("edmcman", 4, false);
    });
  </script>
  <script src="/javascripts/twitter.js" type="text/javascript"> </script>
  
    <a href="http://twitter.com/edmcman" class="twitter-follow-button" data-show-count="false">Follow @edmcman</a>
  
</section>



<section class="googleplus">
  <h1>
    <a href="https://plus.google.com/101696516705586531824?rel=author">
      <img src="http://www.google.com/images/icons/ui/gprofile_button-32.png" width="32" height="32">
      Google+
    </a>
  </h1>
</section>



  
</aside>



    </div>
  </div>
  <footer role="contentinfo"><p>
  Copyright &copy; 2014 - Ed Schwartz -
  <span class="credit">Powered by <a href="http://octopress.org">Octopress</a></span>
</p>

</footer>
  

<script type="text/javascript">
      var disqus_shortname = 'edmcman';
      
        
        // var disqus_developer = 1;
        var disqus_identifier = 'http://edmcman.bitbucket.org/blog/2012/09/17/thoughts-on-coq-and-isabelle-slash-hol/';
        var disqus_url = 'http://edmcman.bitbucket.org/blog/2012/09/17/thoughts-on-coq-and-isabelle-slash-hol/';
        var disqus_script = 'embed.js';
      
    (function () {
      var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
      dsq.src = 'http://' + disqus_shortname + '.disqus.com/' + disqus_script;
      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    }());
</script>



<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#appId=212934732101925&xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>



  <script type="text/javascript">
    (function() {
      var script = document.createElement('script'); script.type = 'text/javascript'; script.async = true;
      script.src = 'https://apis.google.com/js/plusone.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(script, s);
    })();
  </script>



  <script type="text/javascript">
    (function(){
      var twitterWidgets = document.createElement('script');
      twitterWidgets.type = 'text/javascript';
      twitterWidgets.async = true;
      twitterWidgets.src = 'http://platform.twitter.com/widgets.js';
      document.getElementsByTagName('head')[0].appendChild(twitterWidgets);
    })();
  </script>





</body>
</html>
